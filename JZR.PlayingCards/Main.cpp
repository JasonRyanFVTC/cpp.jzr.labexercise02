/*
* Lab Exercise #2 - Playing Cards
* Jason Ryan
* 2020-02-09
*/

#include <iostream>

enum Rank
{
	// Rank value starts from 2
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace, // Ace rank value is 14
};
enum Suit { Diamond, Club, Heart, Spade };

struct Card
{
	Rank Rank;
	Suit Suit;
};

int main()
{
	std::cout << "To be continued...";
	return 0;
}
